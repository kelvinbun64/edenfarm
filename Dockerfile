FROM golang:1.16.14-alpine AS build

RUN apk update && apk add --no-cache git

WORKDIR /app

COPY . .

RUN CGO_ENABLED=0 go build \
    -ldflags "-s -w" \
    -o /out/helloworld ./helloworld.go

FROM gcr.io/distroless/static:latest

WORKDIR /app

COPY --from=build /out/helloworld .

EXPOSE 8080

ENTRYPOINT ["/app/helloworld"]
